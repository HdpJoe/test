package com.ClassLoad;

import java.lang.reflect.Field;

public class Demo {
    public static void main(String[] args) throws ClassNotFoundException, NoSuchFieldException, IllegalAccessException {
        Person person = new Person();
        /*
        获取字节码对象的三种方式
        1.Object类的getClass()方法,判断两个对象是否是同一个字节码文件
        2.静态属性class,锁对象
        3.Class类中静态方法forName()*/

        Class aClass = person.getClass();
        //1.首先得到字节码对象
        Class<Person> aClass1 = Person.class;

        Class aclass2 =Class.forName("com.ClassLoad.Person");
        System.out.println("aclass"+aClass.hashCode());
        System.out.println("aclass"+aClass1.hashCode());
        System.out.println("aclass"+aclass2.hashCode());

        //2.getField(String)方法获取类中的指定字段(可见的)
        Field nameField = aClass.getField("name");
        System.out.println(nameField);
        //3.通过反射给字段赋值、调值
        Person person1 = new Person();
        nameField.set(person1,"kx");
        Object o = nameField.get(person1);
        System.out.println(o);

    }
}
