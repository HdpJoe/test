package com.ClassLoad;

public class Person {
    public String name;
    private float height;

    public Person() {
        System.out.println("无参构造方法");
    }

    public Person(String name, float height) {
        this.name = name;
        this.height = height;
    }

    public void work(){
        System.out.println("正在工作");

    }


}
