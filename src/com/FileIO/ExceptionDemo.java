package com.FileIO;

import java.awt.*;

public class ExceptionDemo {
    public static void main(String[] args) {


        //1.try ...catch
        try{
            int[] arr ={1,2,3};
            System.out.println(arr[2]);
            int a = 10/2;
            int[] arr1 = null;
            System.out.println(arr1[0]);
        }
        catch (ArithmeticException|ArrayIndexOutOfBoundsException e){
            System.out.println(e.getClass());
            System.out.println("子类异常");
        }
        catch (Exception e){
            System.out.println(e.getClass());
            System.out.println("父类处理异常");

        }
    }
}
