package com.FileIO;

import java.io.*;

public class FileDemo {
    public static void main(String[] args) throws IOException {
        /**
         * FileInputStream 从文件系统中的某个文件中获得输入字节
         */
        String path="G:/JavaseWorkspace/MethodDemo/";
        File file = new File(path,"test.txt");
        System.out.println("判断文件是否存在"+file.exists());
        if (file.exists()){
            System.out.println("exist");
            try (FileInputStream fis = new FileInputStream(file)) {//fis得到的是文件字符流
                try {
                    InputStreamReader isr = new InputStreamReader(fis, "utf-8");
                    //isr得到的是文件的字节流
                    BufferedReader br = new BufferedReader(isr);
                    //缓冲流，可以一行行读取
                    String line;
                    while ((line=br.readLine())!=null){
                        System.out.println(line);
                    }
                    br.close();
                    isr.close();
                    fis.close();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        }

        System.out.println("读取文件路径"+file.getPath());

        System.out.println("读取文件绝对路径"+file.getAbsolutePath());

        System.out.println("读取文件父级路径"+new File(file.getAbsolutePath()).getParent());

        System.out.println("读取文件大小"+(float)file.length()/1024+"KB");
        System.out.println("判断文件是否存在"+file.exists());







    }
}

