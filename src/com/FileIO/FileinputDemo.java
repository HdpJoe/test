package com.FileIO;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileinputDemo {
    public static void main(String[] args) throws IOException {
        /**
         * 拷贝图片，每次读取8kb
         */
        //1文件输入流
        FileInputStream fis = null;
        try {
            fis = new FileInputStream("C:/Users/22610/Pictures/Saved Pictures/001.jpg");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        int size = fis.available();//返回文件的长度，字节为单位
        System.out.println("文件大小："+size);
        //2输出流
        FileOutputStream fos = new FileOutputStream("C:/Users/22610/Pictures/Saved Pictures/副本.jpg");
        //3定义个8kb字节数组
        byte[] bytes = new byte[1024 * 8];
        int len =0;
        int i =0;
        while((len = fis.read(bytes))!=-1){
        //4写入文件
            fos.write(bytes,0,len);
            i++;
            System.out.println("读取次数："+i);
        }
        //关闭流
        fis.close();
        fos.close();
    }
}
