package com.Socket;

import java.io.IOException;
import java.net.*;
import java.util.Scanner;

//本地地址 10.209.157.102 端口5288
//目的地址
public class DemoUdp_Sender {
    public static void main(String[] args) throws IOException {
   //1.发送Send,UDP发送数据，直接将数据及源和目的封装成数据包，无需建立连接
//        创建DatagramSocket
        DatagramSocket sender = new DatagramSocket();
        Scanner scanner = new Scanner(System.in);
        while(true) {
            System.out.println("输入内容：");
            String s = scanner.nextLine();
            byte[] bs = s.getBytes("UTF-8");
            //        创建DatagramPacket, 指定数据, 长度, 地址, 端口
            DatagramPacket packet = new DatagramPacket(bs, bs.length, Inet4Address.getByName("10.209.157.102"), 5299);
//        使用DatagramSocket发送DatagramPacket
            sender.send(packet);
//        关闭DatagramSocket
//           sender.close();
        }

    }
}
