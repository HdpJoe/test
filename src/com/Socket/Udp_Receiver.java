package com.Socket;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.Date;

public class Udp_Receiver {
    public static void main(String[] args) throws IOException {
//        2.接收Receive
//        创建DatagramSocket, 指定端口号
        DatagramSocket receiver = new DatagramSocket(5299);
        //定义一个容量杯子
        while (true) {

            byte[] buf = new byte[1024];
            //        创建DatagramPacket, 指定数组, 长度
            DatagramPacket packet = new DatagramPacket(buf, 1024);
            //        使用DatagramSocket接收DatagramPacket
            receiver.receive(packet);
            //        关闭DatagramSocket
            //receiver.close();
            //从DatagramPacket中获取数据
            String s = new String(buf, 0, packet.getLength(), "UTF-8");
            System.out.println(new Date()+"接收到:" + s);
        }
    }
}
