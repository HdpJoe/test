package com.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

/**
 * 判断给定字符串中的括号是否匹配
 */
public class Test {
    private static final Map<Character, Character> brackets = new HashMap<>();
    static {
        brackets.put(')', '(');
        brackets.put(']', '[');
        brackets.put('}', '{');
    }
    public static boolean isMatch(String str) {
        if (str == null) {  
            return false;
        }
        Stack stack = new Stack<>();
        for(char ch:str.toCharArray()){//遍历字符串
            if(brackets.containsValue(ch)) {
                stack.push(ch);
            }else if(brackets.containsKey(ch)){
                if(stack.empty()||stack.pop()!=brackets.get(ch))    {
                    return false;
                }
            }
        }
        return  stack.empty();
    }

}
