package com.Thread;

/**
 * 模拟龟兔赛跑
 */
public class Racer implements Runnable{
    private String winner;//胜利者
    @Override
    public void run() {
        for (int i = 1; i <= 100; i++) {
            System.out.println(Thread.currentThread().getName() + "-->" + i);
            //模拟休息
            if(Thread.currentThread().getName().equals("rabbit")&&i%50==0) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            //比赛是否结束
            boolean flag = gameOver(i);
            if (flag) {
                break;
            }
        }
    }
    private boolean gameOver(int i){
        if (winner!= null) {
            return true;
        }else{
            if (i == 100) {
                winner=Thread.currentThread().getName();
                System.out.println("winner==>"+winner);
                return true;
                }
            }
         return  false;
    }
    public static void main(String[] args) {
        Racer racer = new Racer();
        new Thread(racer,"tortoise").start();
        new Thread(racer,"rabbit").start();
    }
}


