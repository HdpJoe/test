package com.Thread;

import java.util.HashMap;
import java.util.Map;

/**
 *  1.创建：实现Runnable+重写run
 *  2.启动;创建实现类对象+Thread对象+start
 */

public class StartThread implements Runnable {
        public void run() {
            for (int i = 0; i <5; i++) {
                System.out.println("听歌");
            }
        }

    public static void main(String[] args) {

        //创建实现类对象
        StartThread sr = new StartThread();
        //创建代理类对象
        Thread t = new Thread(sr);
        t.start();
        /*某个对象只引用一次，可以使用匿名对象，简单来说，不用声明引用,
         new运算符实例化一个类对象，通过给这个对象分配内存并返回一个指向该内存的引用*/
        //new Thread(new StartThread()).start();
        //匿名内部类
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i <5; i++) {
                    System.out.println("匿名听歌");
                }
            }
        }).start();
        //JDK8简化 lambda表达式
        new Thread(()->{
            for (int i = 0; i <5; i++) {
                System.out.println("匿名听歌");
            }
        }).start();
        for (int i = 0; i < 99; i++) {
            System.out.println("coding");
        }
    }
}
