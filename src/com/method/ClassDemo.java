package com.method;

class Students {
    public void tell() {
        System.out.println("匿名对象调用");
    }
}

class Person {
    private String name;
    static String country;//
    private int age;

    //构造方法
    public Person(String name, int age) {
        this(); //调用本类中的构造方法
        this.name = name;
        this.age = age;
    }

    public Person() {
        System.out.println("无参构造方法");
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setAge(int age) {
        if (age >= 0 && age < 150)
            this.age = age;
    }

    public int getAge() {
        return age;
    }

    public void tell() {
        System.out.println("年龄：" + getAge() + "  " + "姓名:" + getName());
    }
}

public class ClassDemo {
    public static void main(String[] args) {
        String temp = "Hello";
        new Student().tell();  //匿名对象调用
        Person person = new Person("joe", 15);
        Person.country = "beijing";
        person.tell();
        person.setName("kk");
        person.setAge(99);


    }

}
