package com.method;

abstract class Worker {
    private int age;
    public void tell(){
    }
    public abstract void say();
}
class Student extends Person{
 //   private String name;
    private int score;

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    @Override
    public void tell() {
        super.tell(); //调用父类方法
        System.out.println("重写tell");
    }

    public void say(){
        System.out.println("分数："+getScore());
    }
}
public class ExtendsDemo {
    public static void main(String[] args) {
        Student student = new Student();
        student.setName("kj");
        student.setAge(15);
        student.setScore(98);
        student.tell();



    }
}
