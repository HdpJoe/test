package com.method;
;

interface Inter{
    public static final  int AGE = 100; //全局常量
    public abstract void tell();
}
interface Inter2{
    public abstract void say();
}
//定义一个抽象类
abstract class Abs1{
    public abstract void print();
}

//定义子类A去继承抽象类并且实现两个接口
class Abs2 extends Abs1 implements Inter,Inter2{
    @Override
    public void tell() {
    }
    @Override
    public void say() {
    }
    @Override
    public void print() {
    }
}
//接口的多继承
interface Inter3 extends Inter,Inter2{

}

public interface InterDemo {
    public static void main(String[] args) {
        Abs2 abs2 = new Abs2();
        abs2.tell();
        abs2.say();

    }
}
