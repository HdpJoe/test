package com.method;

interface GenInter<T>{
    public void say();
}
class Gin<T> implements GenInter<T>{
    private String info;
    public Gin(String info){
       this.info=info;
   }
    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
   @Override
    public void say() {
    }
    public <T>void pop(T t){//泛型方法

    }
}

class A{
    public void tell1(){
        System.out.println("A--tell1");
    }
    public void tell2(){
        System.out.println("A--tell2");
    }
}
class B extends A {
    public void tell1() {
        System.out.println("B--tell1");
    }

    public void tell3() {
        System.out.println("B--tell3");
    }
}
public class PolDemo01 {
    public static void main(String[] args) {
        //向上转型
        A a = new B();
        a.tell1();//调用的是子类中重写的方法
        a.tell2();
        //向下转型
        B b = (B) a;
        b.tell1();
        b.tell2();
        b.tell3();
        Gin<String> g = new Gin<String>("泛型接口");
        System.out.println(g.getInfo());
        Integer arr[]={1,2,3,57,9};
        tell(arr);
    }
    public static <T> void tell(T arr[]){
        for (int i = 0; i <arr.length ; i++) {
            System.out.println(arr[i]);


        }
    }
}
