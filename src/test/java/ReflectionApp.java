package test.java;

import org.junit.Test;

import java.lang.reflect.Method;

/**
 * 如何获取Class对象
 *1 Class.forName()
 * 2 object.getClass()/class.class
 */

public class ReflectionApp {
    @Test
    public void test01() throws Exception {
        Class<?> clazz = Class.forName("java.lang.Object");
        System.out.println(clazz);
        Method[] methods = clazz.getDeclaredMethods();

        for (Method method:methods) {
            System.out.println(method);
        }
        System.out.println("--------------");
        methods=clazz.getMethods();//拿到所有public方法
        for (Method method:methods) {
            System.out.println(method);
        }
    }
    @Test
    public void test02()throws Exception{
        String name = "kx";
        Class<?> clazz = name.getClass();
        System.out.println(clazz);
        clazz =String.class;
        System.out.println(clazz);
    }
}